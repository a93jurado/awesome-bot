import request from 'superagent';

import { log } from 'decorators';

import { slackConfig } from 'config';

class Slack {
  constructor() {
    this.clientId = slackConfig.CLIENT_ID;
    this.clientSecret = slackConfig.CLIENT_SECRET;
  }

  @log
  authByCode = async ({ code }) => {
    const { body } = await request
      .get(`${slackConfig.API_URL}/oauth.access`)
      .query({ code })
      .query({ 'client_id': this.clientId })
      .query({ 'client_secret': this.clientSecret });

    return body;
  }

  @log
  sendResponse = async ({ response, url }) => {
    return request
      .post(url)
      .send(response);
  }
}

export default new Slack();
