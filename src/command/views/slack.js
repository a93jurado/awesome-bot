const attachmentsView = ({ posts }) => posts.map(({ href, title }) => ({ text: `<${href}|${title}>` }));

const slackMenssageView = ({ posts }) => ({
  text: 'Latest Posts',
  attachments: attachmentsView({ posts }),
});

export default slackMenssageView;
