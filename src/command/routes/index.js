import express from 'express';

import { rebellion } from 'command/handlers';

const commandRoutes = new express.Router();

commandRoutes.route('/').post(rebellion);

export default commandRoutes;
