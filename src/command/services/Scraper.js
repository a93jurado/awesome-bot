import request from 'superagent';
import cheerio from 'cheerio';

import log from 'decorators/log';

class Scraper {
  constructor({ url }) {
    this.url = url;
  }

  @log
  getHtml = async () => {
    const { text = '' } = await request.get(this.url);

    this.html = text;

    return this;
  }

  findBySelector = ({ selector }) => {
    const parsed = cheerio(selector, this.html);
    const { length } = parsed;

    return Array.apply(null, Array(length)).map((_, index) => parsed[index]);
  }
}

export default Scraper;
