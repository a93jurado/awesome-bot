import { getPosts } from 'command/services';
import { slackMenssageView } from 'command/views';
import { slackClient } from 'services';
import { BLOG_URL } from 'config';

const blNews = async ({ responseUrl }) => {
  const posts = await getPosts({ url: BLOG_URL });
  const response = slackMenssageView({ posts: posts.slice(0, 3) });

  return slackClient.sendResponse({ response, url: responseUrl });
};

export default blNews;
