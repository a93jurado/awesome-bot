import Scraper from './Scraper';
import getPosts from './getPosts';
import blNews from './blNews';

export {
  Scraper,
  getPosts,
  blNews,
};
