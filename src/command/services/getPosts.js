import { Scraper } from 'command/services';

const getPosts = async ({ url }) => {
  const scraper = new Scraper({ url });

  const html = await scraper.getHtml();
  const posts = html.findBySelector({ selector: '.post-overlay-block' });

  return posts
    .filter(({ name, attribs }) => name === 'a' && attribs.href)
    .map(post => {
      const { attribs: { href } } = post;
      const [tag] = post.children;
      const [{ data: title }] = tag.children;

      return { href: `${url}${href}`, title };
    });
};

export default getPosts;
