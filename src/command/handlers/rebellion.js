import { blNews } from 'command/services';

const rebellion = async ({ body }, res) => {
  const { text = '', response_url: responseUrl } = body;

  if (text === 'news') {
    blNews({ responseUrl });
  }

  res.json();
};

export default rebellion;
