function log(target, name, descriptor) {
  const original = descriptor.value;

  if (typeof original === 'function') {
    descriptor.value = async function (...args) {
      try {
        const result = await original.apply(this, args);

        return result;
      } catch (e) {
        throw { code: 500, message: e.response || e };
      }
    };
  }

  return descriptor;
}

export default log;
