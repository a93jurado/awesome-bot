import express from 'express';
import bodyParser from 'body-parser';
import '@babel/polyfill';
import morgan from 'morgan';

import logger from 'logger';
import { LOG } from 'config';
import userRoutes from 'user/routes';
import commandRoutes from 'command/routes';

const app = express();
const { PORT = 5000 } = process.env;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

if (LOG) {
  app.use(morgan('combined'));
}

app.get('/', (_, res) => {
  res.send('Slack bot');
});
app.use('/users', userRoutes);
app.use('/commands', commandRoutes);

const start = () => {
  try {
    app.listen(PORT, async () => {
      logger.info(`Running on ${PORT}`);
    });
  } catch (e) {
    logger.error(e);
    process.exit(1);
  }
};

export { app, start };
