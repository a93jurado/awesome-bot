import slackConfig from 'config/slack';

const {
  LOG = 1,
  VERBOSE = 'debug',
  PORT = 5000,
  BLOG_URL,
} = process.env;

export {
  LOG,
  VERBOSE,
  PORT,
  BLOG_URL,
  slackConfig,
};
