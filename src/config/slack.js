const {
  APP_ID,
  CLIENT_ID,
  CLIENT_SECRET,
  API_URL,
  OAUTH_URL,
  REDIRECT_URL,
  SCOPE = 'commands',
} = process.env;

export const slackConfig = {
  APP_ID,
  CLIENT_ID,
  CLIENT_SECRET,
  API_URL,
  OAUTH_URL,
  REDIRECT_URL,
  SCOPE,
};

export default slackConfig;
