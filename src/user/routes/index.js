import express from 'express';

import { auth, install } from 'user/handlers';

const userRoutes = new express.Router();

userRoutes.route('/slack-auth').get(auth);
userRoutes.route('/install').get(install);

export default userRoutes;
