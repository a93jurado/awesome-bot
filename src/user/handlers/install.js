import queryString from 'querystring';
import { slackConfig } from 'config';

const install = (req, res) => {
  const params = {
    client_id: slackConfig.CLIENT_ID,
    redirect_uri: slackConfig.REDIRECT_URL,
    scope: slackConfig.SCOPE,
  };
  const queryParams = queryString.stringify(params);
  const redirectUrl = `${slackConfig.OAUTH_URL}?${queryParams}`;

  res.redirect(301, redirectUrl);
};

export default install;
