import { blAuth } from 'user/services';
import logger from 'logger';

const auth = async ({ query }, res) => {
  try {
    await blAuth(query);

    res.send('Successfully installed');
  } catch (error) {
    logger.error(`Handler user/auth/error: ${error}`);
    res.status(500).json({ message: error.message || error });
  }
};

export default auth;
