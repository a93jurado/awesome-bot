import { slackClient } from 'services';

const blAuth = ({ code }) => {
  if (!code) {
    throw new Error('Missing code parameter');
  }

  return slackClient.authByCode({ code });
};

export default blAuth;
