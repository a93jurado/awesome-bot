import winston from 'winston';
import { VERBOSE } from 'config';

const loggerOptions = {
  level: VERBOSE,
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.simple()
      ),
    }),
  ],
};

export default winston.createLogger(loggerOptions);
