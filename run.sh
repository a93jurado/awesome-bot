#!/usr/bin/env bash

set -a
source services.envar

developmentNet=$(docker network ls -f name=development -q)

if [[ -z $developmentNet ]]; then
  echo "Creating development network"
  docker network create development
fi

echo "Launching slack bot..."
docker-compose up -d --build
