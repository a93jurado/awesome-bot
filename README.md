# Rebellion bot

Slack bot that implements a slash command to scrap post from rebellion blog.

Once you installed this app into your workspace, type `/rebellion news` to get the last three post.

 ## How to run it with docker?

First create a `services.envar` using `service.envar.dist` file as a reference. Copy it and then replace the variables with the right data.



| Environment variable |                                                              |
| -------------------- | ------------------------------------------------------------ |
| NODE_ENV             | `dev`  or  `production`                                      |
| PORT                 | 5000                                                         |
| VERBOSE              | Logging levels in `winston`. Defaults to `info`              |
| BLOG_URL             | Rebellion blog url                                           |
| APP_ID               | Slack App Id                                                 |
| CLIENT_ID            | Slack Client Id                                              |
| CLIENT_SECRET        | Slack Client Secret                                          |
| API_URL              | Slack API url. Defaults to `https://slack.com/api`           |
| OAUTH_URL            | Slack oauth service url. Defaults to: `https://slack.com/oauth/authorize` |
| REDIRECT_URL         | URL  passed in Slack OAuth request. `https://domain-name/users/slack-auth` |



Once this file is created on root project folder, simple run `bash run.sh`. If your system user is nor added in docker group, this command should be run with sudo privileges.

To avoid deploy this app in a hosting platform/webserver, ngrok can be use to set up a secure tunnel to your localhost, and set a proper REDIRECT_URL environment variable.

To install the app into your workspace, simply navigate to `https://domain-name/users/install`